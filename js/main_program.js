"use strict"
window.addEventListener("DOMContentLoaded", function(){enable_disable_Button("#button_Add")});
window.addEventListener("DOMContentLoaded", assignEventForm);
let array_Projects = [];

/** assignEventForm() activates validateElement (in utilities.js) when a form
 * element is blurred.
 */
function assignEventForm(){
    let formProjectId = document.querySelector("#input_ProjectId");
    let formOwnerName = document.querySelector("#input_OwnerName");
    let formTitle = document.querySelector("#input_Title");
    let formCategory = document.querySelector("#select_Category");
    let formHours = document.querySelector("#input_Hours");
    let formRate = document.querySelector("#input_Rate");
    let formStatus = document.querySelector("#select_Status");
    let formShortDescription = document.querySelector("textarea");
    formProjectId.addEventListener("blur", validateElement);
    formOwnerName.addEventListener("blur", validateElement);
    formTitle.addEventListener("blur", validateElement);
    formCategory.addEventListener("blur", validateElement);
    formHours.addEventListener("blur", validateElement);
    formRate.addEventListener("blur", validateElement);
    formStatus.addEventListener("blur", validateElement);
    formShortDescription.addEventListener("blur", validateElement);
}

/** assignResetButton() activates resetForm() when the Reset button is clicked
 */
let initializeResetButton = function assignResetButton(){
    document.querySelector("#button_Reset").addEventListener("click", resetForm);
}
window.addEventListener("DOMContentLoaded", initializeResetButton);

/**resetForm() clears every value in every input of the form, it also activates
 * validateElement() (in utilities) to impede the Add button to still be active
 */
function resetForm(){
    for(let i=0; i < 8; i++){
        if(i==0||i==1||i==2||i==4||i==5){
            document.querySelectorAll(".section_Form")[i].children[1].value = '';
        }else if(i==3||i==6){
            document.querySelectorAll(".section_Form")[i].children[1].value = document.querySelector("select").options[0].value;
        }else{
            document.querySelector("textarea").value = "";
        }
    }
    validateElement();
}

/**assignAddButton() activates addProject() when the Add button is clicked
 */
let initializeAddButton = function assignAddButton(){
    document.querySelector("button").addEventListener("click", addProject);
} 
window.addEventListener("DOMContentLoaded", initializeAddButton);

/** addProject() pushes all the form values into the array_Projects array in 
 * the form of an object. It also calls createTable()
 */
function addProject(){
    let formProjectIdValue = document.querySelector("#input_ProjectId").value;
    let formOwnerNamevValue = document.querySelector("#input_OwnerName").value;
    let formTitleValue = document.querySelector("#input_Title").value;
    let formCategoryValue = document.querySelector("#select_Category").value;
    let formHoursValue = document.querySelector("#input_Hours").value;
    let formRateValue = document.querySelector("#input_Rate").value;
    let formStatusValue = document.querySelector("#select_Status").value;
    let formShortDescriptionValue = document.querySelector("textarea").value;

    array_Projects.push({
        Project_id: formProjectIdValue,
        Owner: formOwnerNamevValue,
        Project_title: formTitleValue,
        Project_category: formCategoryValue,
        Project_status: formStatusValue,
        Number_hours: formHoursValue,
        Rate: formRateValue,
        Project_description: formShortDescriptionValue
    });

    createTable();
}

/** assignWriteButton() runs the writeLocalStorage() function when the Write button is clicked.
 */
function assignWriteButton(){
    document.querySelector("#button_Save").addEventListener("click", writeLocalStorage);
}
window.addEventListener("DOMContentLoaded", assignWriteButton);

/** writeLocalStorage() calls clearLocalStorage() and stringifies each object in the array_Projects, which then are 
 * stored in localStorage. It also posts a message in the readonly input.
 */
let counter = 0;
function writeLocalStorage(){
    clearLocalStorage();
    counter = 0;
    array_Projects.forEach( function(e){
        let value = JSON.stringify(e);
        localStorage.setItem("Slot_"+counter, value);
        counter++;
    })
    document.querySelector("#input_Status").value = localStorage.length+" project(s) has/have been saved in local storage."
}

/** assignAppendButton() runs appendLocalStorage() when the Append button is clicked.
 */
function assignAppendButton(){
    document.querySelector("#button_Append").addEventListener("click", appendLocalStorage);
}
window.addEventListener("DOMContentLoaded", assignAppendButton);

/** appendLocalStorage() stringifies each object in the array and adds them to localStorage. It also posts
 * a message in the readonly input.
 */
function appendLocalStorage(){
    array_Projects.forEach( function(e){
        let value = JSON.stringify(e);
        localStorage.setItem("Slot_"+counter, value);
        counter++;
    })
    document.querySelector("#input_Status").value = localStorage.length+" project(s) has/have been appended in local storage."
}

/** assignClearButton() runs the clearLocalStorage() function when the Clear button is clicked.
 */
 function assignClearButton(){
    document.querySelector("#button_Clear").addEventListener("click", clearLocalStorage);
}
window.addEventListener("DOMContentLoaded", assignClearButton);

/** clearLocalStorage() clears the localStorage of all data and posts a message in the readonly input.
 */
function clearLocalStorage(){
    localStorage.clear();
    document.querySelector("#input_Status").value = "All projects in local storage have been cleared."
}

/** assignLoadButton() runs loadLocalStorage() when the Load button is clicked.
 */
 function assignLoadButton(){
    document.querySelector("#button_Load").addEventListener("click", loadLocalStorage);
}
window.addEventListener("DOMContentLoaded", assignLoadButton);

/** loadLocalStorage() parses the items in localStorage, puts them in the array_Projects array which is
 * used to create a new table with the new objects. A message is also created in the readonly input.
 */
function loadLocalStorage(){
    array_Projects = [];
    for(let i = 0; i < localStorage.length; i++){
        let storage = localStorage.getItem("Slot_"+i);
        let value = JSON.parse(storage); 
        array_Projects.push(value);
    }
    createTable();
    document.querySelector("#input_Status").value = "There is/are "+localStorage.length+" project(s) in local storage."
}

/** assignSearch() runs search() when the Query (readonly)input is blurred.
 */
function assignSearch(){
    document.querySelector("#input_Query").addEventListener("blur", search);
}
window.addEventListener("DOMContentLoaded", assignSearch);

/** search() searches for exact matching terms in the array_Search array (which is created as a copy of the
 * array_Projects array through .map) and removes the entries on the table if there is no match. Inputting ""
 * makes search() do nothing. All three options have messages for the readonly input as well.
 */
function search(){
    let term = document.querySelector("#input_Query").value;
    createTable();
    let array_Search = array_Projects.map((e)=>e);
    let j = 0;
    for(let i = 0; i < array_Search.length; i++){
    if(term==array_Search[i+j].Project_id||term==array_Search[i+j].Owner||
        term==array_Search[i+j].Project_title||term==array_Search[i+j].Project_category||
        term==array_Search[i+j].Project_status||term==array_Search[i+j].Number_hours||
        term==array_Search[i+j].Rate||term==array_Search[i+j].Project_description){
            document.querySelector("#input_Status").value = "There is/are "+(i+1)+" project(s) that match your query."
    }else if(term==""){
        document.querySelector("#input_Status").value = "Showing all projects."
    }else{
        document.querySelectorAll("tr")[i+1].remove();
        --i;
        document.querySelector("#input_Status").value = "There is/are "+(i+1)+" project(s) that match your query."
        j++;
    }
    }
}

/** assignSortButton() runs sort() when the Sort button is clicked.
 */
function assignSortButton(){
    document.querySelector("#button_Sort").addEventListener("click", sort);
}
window.addEventListener("DOMContentLoaded", assignSortButton);

/** compare(a,b) returns a number that sort() uses when comparing the numbers in each Object.Project_id
 * in each slot of the array
 * @param {string} a represents an Object.Project_id
 * @param {string} b represents another Object.Project_id
 * @returns {number}
 */
function compare(a,b){
    if(a.Project_id < b.Project_id){
        return -1;
    }
    if(a.Project_id > b.Project_id){
        return 1;
    }
    return 0;
}

/** sort() calls .sort() on array_Projects using the number value from compare() to determine what it does.
 * > 0 sorts b before a, < 0 sorts a before b, == 0 doesn't affect the sorting process.
 */
function sort(){
    array_Projects.sort(compare);
    createTable();
}
