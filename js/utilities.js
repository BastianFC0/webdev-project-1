'use strict'
/** enable_disable_Button(string) is a utility function used to enable and disable
 * buttons. It is used by other functions such as validateElement().
 * @param {string} button_id id of a button
 */
function enable_disable_Button(button_id){
    let  button = document.querySelector(button_id);
    if(button.disabled==false){
        button.disabled=true;
        button.style.backgroundColor="rgb("+203+","+203+","+203+")";
    } else {
        button.disabled=false;
        button.style.backgroundColor="rgb("+32+","+105+","+173+")";
    }
}
/** clear() deletes everything that comes after the first two elements of
 * the .section_Form class. It is used in the validateElement() function for
 * it to be consistent.
 */
function clear(){
let counter = 0;
for(let i=0; i < 8; i++){
    counter=document.querySelectorAll(".section_Form")[i].childElementCount;
    if(counter>2){
        for(let j=counter; j > 2; j--){
        document.querySelectorAll(".section_Form")[i].lastChild.remove();
            }
        }
    }
}
/** addError(string, number) adds an error message as the last child of the
 * indicated (by its index) .section_Form and then adds the Interdiction image
 * before the error message.
 * @param {string} name name of the label of the chosen form input
 * @param {number} index index number to select the correct .section_Form
 */
 function addError(name, index){
    let error = document.createElement("p");
    let errorMessage = document.createTextNode(`Wrong format for ${name}`);
    error.appendChild(errorMessage);
    let position = document.querySelectorAll(".section_Form")[index];
    position.appendChild(error);
    /**code to add the Interdiction image*/
    let icon = document.createElement("img");
    icon.src="../images/Interdiction.png";
    position.insertBefore(icon, position.lastChild);
}
/** addCorrect(number) adds the Correct.png image as the lastChild of the
 * selected (by its index) .section_Form.
 * @param {number} index index number to select the correct .section_Form
 */
function addCorrect(index){
    let icon = document.createElement("img");
    icon.src="../images/Correct.png";
    document.querySelectorAll(".section_Form")[index].appendChild(icon);
}
/** validateElement() uses regex to test whether the values of the inputs in the
 * form are correct (since there were no specifications for them I guessed by the 
 * existing projects in the pdf instructions). If they are correct, then 1 gets
 * added to a counter. If the counter reaches 8, the button activates. There is
 * also a check at the end if the user mistakenly enters an incorrect value to
 * re-disable the button.
 */
function validateElement(){
    clear();
    let counter = 0;
    let regex = {
        regexProjectId: /^proj_\d+$/,
        regexOwnerName: /^[A-Za-z]{2}[A-Za-z_]+$/,
        regexTitleShortDescription: /^\w/,
        regexHoursRate: /^\d/,
        regexSelects: /^(?!----------)/
    };
    let stringProjectId = document.querySelector("#input_ProjectId").value;
        if(regex.regexProjectId.test(stringProjectId)){
            addCorrect(0);
            ++counter;
        }else{
            addError("Project id", 0);
        }
    let stringOwnerName = document.querySelector("#input_OwnerName").value;
        if(regex.regexOwnerName.test(stringOwnerName)){
            addCorrect(1);
            ++counter;
        }else{
            addError("Owner Name", 1);
        }
    let stringTitle = document.querySelector("#input_Title").value;
        if(regex.regexTitleShortDescription.test(stringTitle)){
            addCorrect(2);
            ++counter;
        }else{
            addError("Title", 2);
        }
    let stringCategory = document.querySelector("#select_Category").value;
        if(regex.regexSelects.test(stringCategory)){
            addCorrect(3);
            ++counter; 
        }else{
            addError("Category", 3);
        }
    let numberHours = document.querySelector("#input_Hours").value;
        if(regex.regexHoursRate.test(numberHours)){
            addCorrect(4);
            ++counter;
        }else{
            addError("Hours", 4);
        }
    let numberRate = document.querySelector("#input_Rate").value;
        if(regex.regexHoursRate.test(numberRate)){
            addCorrect(5);
            ++counter;
        }else{
            addError("Rate", 5);
        }
    let stringStatus = document.querySelector("#select_Status").value;
        if(regex.regexSelects.test(stringStatus)){
            addCorrect(6);
            ++counter;
        }else{
            addError("Rate", 6);
        }
    let stringShortDescription = document.querySelector("textarea").value;
        if(regex.regexTitleShortDescription.test(stringShortDescription)){
            addCorrect(7);
            ++counter;
        }else{
            addError("Short Description", 7);
        }
    if(counter == 8 && document.querySelector("#button_Add").disabled == true){
        enable_disable_Button("#button_Add");
    }
    if(counter < 8 && document.querySelector("#button_Add").disabled == false){
        enable_disable_Button("#button_Add");
    }
}
