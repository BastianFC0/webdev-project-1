'use strict'
/** createTable() creates the trs and tds, populates tds with values from the 
 * array_Projects array and changes the backgroundColor of tds if they are in
 * pair trs. It also deletes projects in the array and in the table upon clicking
 * the trash image. The functionality for the edit image is also included (and it 
 * took me a long time to make it work).
 */
 function createTable(){
    for(let i = 1; i < document.querySelector("tbody").children.length;){
        document.querySelector("tbody").lastElementChild.remove();
    }
    const lastRow =  document.querySelector("tbody");
    for(let i = 0; i < array_Projects.length; i++){
        let tableRow = document.createElement("tr");
        lastRow.append(tableRow);
    for(let j = 0; j < 10; j++){
        let tableCell = document.createElement("td");
        let tableIndex = document.querySelectorAll("tr")[i+1];
        tableIndex.appendChild(tableCell);
        let counter = i*10+j;
        let tableInnerCell = document.querySelectorAll("td")[counter];
            if(j==0){
                tableInnerCell.innerHTML = array_Projects[i].Project_id;
                if(i%2){
                    tableInnerCell.style.backgroundColor="white";
                }
            }
            if(j==1){
                tableInnerCell.innerHTML = array_Projects[i].Owner;
                if(i%2){
                    tableInnerCell.style.backgroundColor="white";
                }
            }
            if(j==2){
                tableInnerCell.innerHTML = array_Projects[i].Project_title;
                if(i%2){
                    tableInnerCell.style.backgroundColor="white";
                }
            }
            if(j==3){
                tableInnerCell.innerHTML = array_Projects[i].Project_category;
                if(i%2){
                    tableInnerCell.style.backgroundColor="white";
                }
            }
            if(j==4){
                tableInnerCell.innerHTML = array_Projects[i].Project_status;
                if(i%2){
                    tableInnerCell.style.backgroundColor="white";
                }
            }
            if(j==5){
                tableInnerCell.innerHTML = array_Projects[i].Number_hours;
                if(i%2){
                    tableInnerCell.style.backgroundColor="white";
                }
            }
            if(j==6){
                tableInnerCell.innerHTML = array_Projects[i].Rate;
                if(i%2){
                    tableInnerCell.style.backgroundColor="white";
                }
            }
            if(j==7){
                tableInnerCell.innerHTML = array_Projects[i].Project_description;
                if(i%2){
                    tableInnerCell.style.backgroundColor="white";
                }
            }
            if(j==8){
                let img = document.createElement("img");
                img.src = "../images/Edit.png"
                tableInnerCell.appendChild(img);
                img.addEventListener("click", function edit(){
                    img.src = "../images/Save.png";
                    for(let h = 0; h < 8; h++){
                        let valueInput = tableIndex.children[h].innerHTML;
                        let input = document.createElement("input");
                        input.setAttribute("type","text");
                        input.setAttribute("value",valueInput);
                        tableIndex.children[h].innerHTML="";
                        tableIndex.children[h].append(input);
                    }
                    img.removeEventListener("click", edit);
                    img.addEventListener("click", function save(){
                        img.src = "../images/Edit.png";
                    for(let k = 0; k < 8; k++){
                        let inputValue = tableIndex.children[k].children[0].value;
                        tableIndex.children[k].children[0].remove();
                        tableIndex.children[k].append(inputValue);
                        if(k==0){
                            array_Projects[i].Project_id = inputValue;
                        }
                        if(k==1){
                            array_Projects[i].Owner = inputValue;
                        }
                        if(k==2){
                            array_Projects[i].Project_title = inputValue;
                        }
                        if(k==3){
                            array_Projects[i].Project_category = inputValue;
                        }
                        if(k==4){
                            array_Projects[i].Project_status = inputValue;
                        }
                        if(k==5){
                            array_Projects[i].Number_hours = inputValue;
                        }
                        if(k==6){
                            array_Projects[i].Rate = inputValue;
                        }
                        if(k==7){
                            array_Projects[i].Project_description = inputValue;
                        }
                    }
                        img.removeEventListener("click", save);
                        img.addEventListener("click", edit);
                    });
                });
                if(i%2){
                    tableInnerCell.style.backgroundColor="white";
                }
            }
            if(j==9){
                let img = document.createElement("img");
                img.src = "../images/Delete.png"
                tableInnerCell.appendChild(img);
                img.addEventListener("click", function(){
                    if(confirm("Do you want to remove "+array_Projects[i].Project_id)){
                    tableIndex.remove()
                    array_Projects.splice(i,1);
                    }
                });
                if(i%2){
                    tableInnerCell.style.backgroundColor="white";
                }
            }
        }
    
    }
}