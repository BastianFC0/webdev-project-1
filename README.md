The program runs as follows:
1. You input a "project" by filling out the form.
2. You click on the "Add" button, which adds your project as an object in an array that will create and populate a table that is visible below the form.
3. You can add as many "projects" as you want
4. You can then sort them if you wish by clicking on the "Sort" button visible below the table
5. You then can Overwrite, Append, Clear and Load your projects into/from localStorage if you want the browser to memorize them
6. You can also search for specific projects on the search bar, but note that doing step 5 during this step will not work since the array created for the search is not the array used for all the functions in step 5.
7. You can delete and edit projects directly from the table by clicking on the images depicting the former operations.
